package com.mycompany.chainofresponsibilty_java;
// Define an abstract class for the Approver in the chain.
abstract class Approver {
    protected Approver nextApprover; // Reference to the next Approver in the chain
    protected double approvalLimit; // The maximum amount this Approver can approve
    public void setNextApprover(Approver nextApprover) {
        this.nextApprover = nextApprover; // Set the next Approver in the chain
    }
    public void processRequest(double amount) {
        if (amount <= approvalLimit) {
            approveRequest(); // If the amount is within the limit, approve the request
        } else if (nextApprover != null) {
            nextApprover.processRequest(amount); // Pass the request to the next Approver
        } else {
            System.out.println("Request denied by all approvers."); // If no Approver can approve, deny the request
        }
    }
    abstract protected void approveRequest(); // Abstract method to be implemented by concrete Approvers
}
// Concrete Approver class for a Manager
class Manager extends Approver {
    Manager() {
        approvalLimit = 1000; // Manager's approval limit
    }
    protected void approveRequest() {
        System.out.println("Manager has approved the request."); // Approval action for Manager
    }
}
// Concrete Approver class for a Director
class Director extends Approver {
    Director() {
        approvalLimit = 5000; // Director's approval limit
    }
    protected void approveRequest() {
        System.out.println("Director has approved the request."); // Approval action for Director
    }
}

// Concrete Approver class for a CEO
class CEO extends Approver {
    CEO() {
        approvalLimit = 10000; // CEO's approval limit
    }
    protected void approveRequest() {
        System.out.println("CEO has approved the request."); // Approval action for CEO
    }
}
public class ChainOfResponsibilty_java {
    public static void main(String[] args) {
       Manager manager = new Manager();
        Director director = new Director();
        CEO ceo = new CEO();
        manager.setNextApprover(director); // Set up the chain of responsibility
        director.setNextApprover(ceo);
        double purchaseAmount = 6000;
        manager.processRequest(purchaseAmount); // Process a purchase request
    }
}
